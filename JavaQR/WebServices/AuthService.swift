//
//  AuthService.swift
//  JavaQR
//
//  Created by Intrazero on 09/01/2024.
//

import Foundation
import Alamofire
import FirebaseAuth

// AuthServiceProtocol
protocol AuthServiceProtocol {
    
    func sendOTP(countryCode: String, phone: String, completion: @escaping (Error?) -> ())
    func verifyOTP(with verificationCode: String, completion: @escaping (Error?) -> ())
    func register(newUser: User, completion: @escaping (Bool, AuthModel?, Error?) -> ())
    func login(user: User, completion: @escaping (Bool, AuthModel?, Error?) -> ())
}

class AuthService : AuthServiceProtocol {
    
    // MARK: - Send OTP
    
    func sendOTP(countryCode: String, phone: String, completion: @escaping (Error?) -> ()) {
        let phoneWithCode = countryCode + phone
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneWithCode, uiDelegate: nil) { verificationID, error in
            if let verificationID = verificationID {
                UserDefaults.standard.set(verificationID, forKey: K.Key.authID)
                completion(nil)
            } else {
                completion(error)
            }
        }
    }
    
    // MARK: - Verify OTP
    
    func verifyOTP(with verificationCode: String, completion: @escaping (Error?) -> ()) {
        guard let verificationID = UserDefaults.standard.string(forKey: K.Key.authID)
        else {
            completion(AuthErrorCode.missingVerificationID as? Error)
            return
        }
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { result, error in
            completion(error)
        }
    }
    
    // MARK: - Register User
    
    func register(newUser: User, completion: @escaping (Bool, AuthModel?, Error?) -> ()) {
        AF.request("\(K.Api.url)\(K.Api.register)",
                   method: .post,
                   parameters: newUser,
                   encoder: URLEncodedFormParameterEncoder(destination: .httpBody),
                   headers: K.Api.headers)
        .responseDecodable(of: AuthModel.self) { response in
            switch response.result {
            case .success(let json):
                completion(true, json, nil)
            case .failure(let error):
                completion(false, nil, error)
            }
        }
    }
    // MARK: - Login User
    
    func login(user: User, completion: @escaping (Bool, AuthModel?, Error?) -> ()) {
        AF.request("\(K.Api.url)\(K.Api.login)",
                   method: .post,
                   parameters: user,
                   encoder: URLEncodedFormParameterEncoder(destination: .httpBody),
                   headers: K.Api.headers)
        .responseDecodable(of: AuthModel.self) { response in
            switch response.result {
            case .success(let json):
                completion(true, json, nil)
            case .failure(let error):
                completion(false, nil, error)
            }
        }
    }
}
