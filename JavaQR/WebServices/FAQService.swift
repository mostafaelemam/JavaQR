//
//  FAQservice.swift
//  JavaQR
//
//  Created by Intrazero on 31/12/2023.
//

import Foundation
import Alamofire

protocol FaqServiceProtocol {
    func getFAQs(completion: @escaping (_ success: Bool, _ statusCode: Int?, _ results: FAQData?) -> Void)
}

class FAQService : FaqServiceProtocol {
    
    func getFAQs(completion: @escaping (Bool, Int?, FAQData?) -> Void) {
        
        AF.request("\(K.Api.url)\(K.Api.faqs)",
                   method: .get ,
                   headers: K.Api.headers)
        .validate(statusCode: 200..<300)
        .responseDecodable(of: FAQData.self) { response in
                switch response.result {
                case .success(let mainJSON):
                    DispatchQueue.main.async {
                        completion(true, nil, mainJSON)
                    }
                case .failure(let error):
                    let statusCode = response.response?.statusCode ?? 0
                    print(error.localizedDescription.debugDescription)
                    completion(false, statusCode, nil)
                }
            }
    }
}

