//
//  GiftService.swift
//  JavaQR
//
//  Created by Intrazero on 01/01/2024.
//

import Foundation
import Alamofire

// GiftServiceProtocol
protocol GiftServiceProtocol {
    func getGifts(completion: @escaping (_ success: Bool, _ results: GiftData?) -> Void)
}
class GiftService : GiftServiceProtocol {
    
    func getGifts(completion: @escaping (Bool, GiftData?) -> Void) {
        
        AF.request("\(K.Api.url)\(K.Api.gifts)", method: .get , headers: K.Api.headers)
            .responseDecodable(of: GiftData.self) { response in
                switch response.result {
                case .success(let mainJSON):
                    DispatchQueue.main.async {
                        completion(true, mainJSON)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }
    }
}
