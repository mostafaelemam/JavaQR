//
//  GiftViewModel.swift
//  JavaQR
//
//  Created by Intrazero on 01/01/2024.
//

import Foundation

class GiftViewModel {
    private let giftServiceProtocol : GiftServiceProtocol
    init(giftServiceProtocol: GiftServiceProtocol) {
        self.giftServiceProtocol = giftServiceProtocol
    }
    var giftsData : GiftData?

    var responseSuccessCallback: (() -> Void)?
    var responseFailedCallback: (() -> Void)?
    
    func getGifts() {
        giftServiceProtocol.getGifts { success, results in
            if success {
                self.giftsData = results
                self.responseSuccessCallback?()
            } else {
                self.responseFailedCallback?()
            }
        }
    }
}
