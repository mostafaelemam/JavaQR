//
//  FAQViewModel.swift
//  JavaQR
//
//  Created by Intrazero on 31/12/2023.
//

import Foundation

class FAQViewModel {
    // MARK: - Initialization
    
    private let faqServiceProtocol : FaqServiceProtocol
    init(faqServiceProtocol: FaqServiceProtocol) {
        self.faqServiceProtocol = faqServiceProtocol
    }
    // MARK: - Getting FAQs
    
    var faqData : FAQData?
    var responseSuccessCallback: (() -> Void)?
    var responseFailedCallback: (() -> Void)?
    
    func getFaqs() {
        faqServiceProtocol.getFAQs { success, statusCode, results  in
            if success {
                self.faqData = results
                self.responseSuccessCallback?()
            } else {
                switch statusCode! {
                case 400...499:
                    MyController.showBanner(title: Localized.error, message: Localized.unauthorized)
                default:
                    MyController.showBanner(title: Localized.error, message: Localized.faqsNotFound)
                }
                self.responseFailedCallback?()
            }
        }
    }
}
