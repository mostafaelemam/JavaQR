//
//  File.swift
//  JavaQR
//
//  Created by Intrazero on 09/01/2024.
//

import Foundation
import FirebaseAuth

class AuthViewModel {
    private let authService: AuthServiceProtocol
    
    // MARK: - Initialization
    
    init(authService: AuthServiceProtocol = AuthService()) {
        self.authService = authService
    }
    
    // MARK: - Sending OTP
    
    func sendOTP(countryCode: String, phone: String, completion: @escaping (Error?) -> Void) {
        authService.sendOTP(countryCode: countryCode, phone: phone) { error in
            if let error = error as? AuthErrorCode {
                switch error.code {
                case .webContextCancelled:
                    MyController.showBanner(title: Localized.error, message: Localized.interactionCancelled)
                    
                case .invalidPhoneNumber:
                    MyController.showBanner(title: Localized.error, message: Localized.invalidPhone)
                    
                default:
                    MyController.showBanner(title: Localized.error, message: Localized.somethingWrong)
                }
                completion(error)
            } else {
                completion(nil)
            }
        }
    }
    // MARK: - OTP Verification
    
    func verifyOTP(with verificationCode: String, completion: @escaping (Error?) -> Void) {
        authService.verifyOTP(with: verificationCode, completion: completion)
    }
    // MARK: - Register
    
    func register(user: User, completion: @escaping (Bool, AuthModel?, Error?) -> Void) {
        authService.register(newUser: user) { success, authModel,error in
            if let error = error {
                MyController.showBanner(title: Localized.error, message:  error.localizedDescription, duration: 4)
                completion(false, nil, error)
                
            } else if success {
                if let user = authModel?.user, let token = authModel?.tokenData?.accessToken {
                    UserDefaults.standard.setValue(user.firstName ?? "", forKey: K.Key.firstName)
                    UserDefaults.standard.setValue(user.lastName ?? "", forKey: K.Key.lastName)
                    UserDefaults.standard.setValue(user.mobile ?? "", forKey: K.Key.mobile)
                    UserDefaults.standard.setValue(user.countryCode ?? "", forKey: K.Key.countryCode)
                    UserDefaults.standard.setValue(token, forKey: K.Key.token)
                    K.Api.headers["Authorization"] = "Bearer " + (token)
                    
                    completion(true, authModel, nil)
                    
                } else if let failedError = authModel?.errors?.values.first?.first {
                    MyController.showBanner(title: Localized.error, message: failedError, duration: 4)
                    completion(false, authModel, nil)
                }
            }
        }
    }
    // MARK: - Login
    
    func login(user: User, completion: @escaping (Bool, AuthModel?, Error?) -> Void) {
        authService.login(user: user) { success, authModel, error in
            if let error = error {
                MyController.showBanner(title: Localized.error, message: error.localizedDescription, duration: 4)
                completion(false, nil, error)
                
            } else if success {
                if let token = authModel?.tokenData?.accessToken {
                    UserDefaults.standard.setValue(token, forKey: K.Key.token)
                    completion(true, authModel, nil)
                    
                } else if let failedError = authModel?.errors?.values.first?.first {
                    MyController.showBanner(title: Localized.error, message: failedError, duration: 4)
                    completion(false, authModel, nil)
                }
            }
        }
    }
}
