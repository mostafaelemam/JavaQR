//
//  Gift.swift
//  JavaQR
//
//  Created by Intrazero on 01/01/2024.
//

import Foundation

struct GiftData : Codable {
    let success: Bool
    let message: String
    var data : Gifts
}
struct Gifts : Codable {
    var data : [Gift]
}

struct Gift : Codable {
    let name: String
    let description: String
    let image: String
    let remainingPoints: Int
    
    enum CodingKeys: String, CodingKey {
        case name, description, image
        case remainingPoints = "remaining_points"
    }
    
}


