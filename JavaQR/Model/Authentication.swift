//
//  Register.swift
//  JavaQR
//
//  Created by Intrazero on 01/01/2024.
//
import Foundation

struct AuthModel : Codable {
    let user: User?
    let tokenData: TokenData?
    let message: String?
    let errors : [String : [String]]?
    
    enum CodingKeys : String, CodingKey {
        case user, message, errors
        case tokenData = "token_data"
    }
}
struct User : Codable {
    let firstName   : String?
    let lastName    : String?
    let countryCode : String?
    let mobile      : String?
    let fcm         : String?
    let loginMethod : String? = "Otp"

    enum CodingKeys: String, CodingKey {
        case firstName   = "first_name"
        case lastName    = "last_name"
        case countryCode = "country_code"
        case loginMethod = "login_method"
        case mobile, fcm
    }
}
struct TokenData : Codable {
    let accessToken, refreshToken: String
    enum CodingKeys: String, CodingKey {
        case accessToken   = "access_token"
        case refreshToken  = "refresh_token"
    }
}
