//
//  FAQ.swift
//  JavaQR
//
//  Created by Intrazero on 31/12/2023.
//

import Foundation

struct FAQData : Codable {
    let success: Bool
    let message: String
    var data : FAQs
}
struct FAQs : Codable {
    var data : [FAQ]
}

struct FAQ : Codable {
    let question, answer : String?
    var isExpanded: Bool

    init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CodingKeys.self)
           question = try container.decode(String.self, forKey: .question)
           answer = try container.decode(String.self, forKey: .answer)

           // Default value for isExpandedProperty is false , As it is not Found in Json Response
            isExpanded = false
       }
    
    

}
