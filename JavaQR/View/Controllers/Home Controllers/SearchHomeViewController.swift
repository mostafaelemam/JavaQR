//
//  SearchHomeViewController.swift
//  JavaQR
//
//  Created by Intrazero on 18/12/2023.
//

import UIKit

class SearchHomeViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBarCustomized()
        tableView.register(PointTableViewCell.nib, forCellReuseIdentifier: PointTableViewCell.identifier)
        if let navigationController = navigationController {
            navigationController.navigationBar.barStyle = .black // This will set the navigation bar style
            navigationController.navigationBar.tintColor = .white
        }
        }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = .black
    }
    private func searchBarCustomized() {
        searchBar.barTintColor = .black
        searchBar.setImage(UIImage(systemName: "xmark"), for: .clear, state: .normal)
        searchBar.setImage(UIImage(named: "search-icon"), for: .search , state: .normal)
        
        if let textInSearchBar = searchBar.value(forKey: "searchField") as? UITextField {
            textInSearchBar.textColor = .white
            textInSearchBar.placeholderColor = .white
            textInSearchBar.tintColor = .white
            textInSearchBar.font = UIFont(name: "Inter-Regular_Medium", size: 16)
        }
    }
}
// MARK: - TableView DataSource
extension SearchHomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PointTableViewCell.identifier, for: indexPath) as! PointTableViewCell
        cell.contentView.backgroundColor = .white
        return cell
    }
}
