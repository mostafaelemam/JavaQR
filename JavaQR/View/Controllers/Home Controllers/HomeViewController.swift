//
//  HomeViewController.swift
//  JavaQR
//
//  Created by Intrazero on 17/12/2023.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var shopsCollectionView: UICollectionView!
    @IBOutlet weak var pointTableView: UITableView!
    
    var shopLabelText = ""
    var shopImage = UIImage()
    var price = ""
    
    var itemLabelText = ""
    var itemImage = UIImage()
    
    let points = [10,6,6,6,7,8,10,6]
    let pointsHistory = [431,-431,431,-431]

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shopVC = segue.destination as? ShopViewController {
            shopVC.shopName = shopLabelText
            shopVC.image = shopImage
            shopVC.price = price
        }
    }
    private func setup() {
//        Registering Nibs
        itemsCollectionView.register(ItemCollectionViewCell.nib, forCellWithReuseIdentifier: ItemCollectionViewCell.identifier)
        shopsCollectionView.register(ShopCollectionViewCell.nib, forCellWithReuseIdentifier: ShopCollectionViewCell.identifier)
        pointTableView.register(PointTableViewCell.nib, forCellReuseIdentifier: PointTableViewCell.identifier)
    }
}
// MARK: - CollectionView Datasource
extension HomeViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == itemsCollectionView {
            return points.count
        } else {
            return 4
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == itemsCollectionView {
            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.identifier, for: indexPath) as! ItemCollectionViewCell
            if points[indexPath.row] == 10 {
                itemCell.bottomView.isHidden = true
                itemCell.getBtn.isHidden = false
            } else {
                itemCell.bottomView.isHidden = false
                itemCell.getBtn.isHidden = true
            }
            return itemCell
        } else {
            let shopCell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopCollectionViewCell.identifier, for: indexPath) as! ShopCollectionViewCell
            return shopCell
        }
    }
}
// MARK: - Collection View Delegate
extension HomeViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == shopsCollectionView {
            if let shopCell = collectionView.cellForItem(at: indexPath) as? ShopCollectionViewCell {
                shopLabelText = shopCell.shopName.text!
                shopImage = shopCell.shopImageView.image!
                price = shopCell.points.text!
                performSegue(withIdentifier: K.Segue.shop, sender: self)
            }
        } else if collectionView == itemsCollectionView {
            if let itemCell = collectionView.cellForItem(at: indexPath) as? ItemCollectionViewCell {
                itemLabelText = itemCell.itemLabel.text!
                itemImage = itemCell.itemImageView.image!
                performSegue(withIdentifier: K.Segue.item, sender: self)
                
            }
        }
    }
}
// MARK: - TableView Datasource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointsHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PointTableViewCell.identifier, for: indexPath) as! PointTableViewCell
        if pointsHistory[indexPath.row] > 0 {
            cell.detailsLabel.text = "Earned Points"
            cell.pointsLabel.text = "+\(pointsHistory[indexPath.row]) Points"
            cell.pointsLabel.textColor = UIColor(named: "Success-300")
        } else {
            cell.detailsLabel.text = "Iced Caffe Latte"
            cell.pointsLabel.text = "\(pointsHistory[indexPath.row]) Points"
            cell.pointsLabel.textColor = UIColor(named: "Warning-300")
        }
        return cell
    }
}
