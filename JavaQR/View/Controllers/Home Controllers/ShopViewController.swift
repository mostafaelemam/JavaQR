//
//  ShopViewController.swift
//  JavaQR
//
//  Created by Intrazero on 20/12/2023.
//

import UIKit
import Lottie
import Kingfisher

class ShopViewController: UIViewController {
    
    @IBOutlet weak var ordersCollectionView: UICollectionView!
    @IBOutlet weak var giftsTableView: UITableView!
    
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var shopLabel: UILabel!
    @IBOutlet weak var pricerPerPoint: UILabel!
    @IBOutlet weak var shopID: UILabel!
    
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var collectPointsView: UIView!
    @IBOutlet weak var newPointsAnimation: UIView!
    
    private var giftViewModel : GiftViewModel?
    private var gifts : GiftData?
    
    var animationView = LottieAnimationView.init()
    var image = UIImage()
    var shopName = ""
    var price = "23 SAR/Point"
    let pointsArr = [6,7,10,6,7,10,8]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyController.roundedOnlyTop(for: newPointsAnimation)
        setup()
        loadGifts()
    }
    func setup() {
        shopImageView.image = image
        shopLabel.text = shopName
        pricerPerPoint.text = price
        //        Registering Nib
        giftsTableView.register(GiftTableViewCell.nib, forCellReuseIdentifier: GiftTableViewCell.identifier)
        ordersCollectionView.register(ItemCollectionViewCell.nib, forCellWithReuseIdentifier: ItemCollectionViewCell.identifier)
    }
    private func loadGifts() {
        giftViewModel = GiftViewModel(giftServiceProtocol: GiftService())
        if let giftVM = giftViewModel {
            giftVM.getGifts()
            giftVM.responseSuccessCallback = { [weak self] in
                self?.gifts = giftVM.giftsData
                self?.giftsTableView.reloadData()
            }
            giftVM.responseFailedCallback = {
                print("ERROR Getting Gifts")
            }
        }
    }
    @IBAction func scanBtnPressed(_ sender: UIBarButtonItem) {
        tabBarController?.tabBar.isUserInteractionEnabled = false
        blurView.alpha = 0.85
        self.collectPointsView.alpha = 1
        self.animationView.animation = .named("new earn points")
        self.animationView.frame = CGRect(x: 0, y: 0, width: collectPointsView.bounds.width, height: 425)
        self.animationView.loopMode = .playOnce
        self.animationView.contentMode = .scaleAspectFill
        self.newPointsAnimation.addSubview(self.animationView)
        self.animationView.play()
    }
    @IBAction func collectBtnPressed(_ sender: UIButton) {
        blurView.alpha = 0
        collectPointsView.alpha = 0
        tabBarController?.tabBar.isUserInteractionEnabled = true

    }
}
// MARK: - CollectionView
extension ShopViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return pointsArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.identifier, for: indexPath) as! ItemCollectionViewCell
        if pointsArr[indexPath.row] == 10 {
            cell.bottomView.isHidden = true
            cell.getBtn.isHidden = false
            
        } else {
            cell.bottomView.isHidden = false
            cell.getBtn.isHidden = true
        }
        return cell
    }
}
// MARK: - Gifts TableView
extension ShopViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gifts?.data.data.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GiftTableViewCell.identifier, for: indexPath) as! GiftTableViewCell
        guard let allGifts = self.gifts?.data.data else { fatalError("No Gifts") }
        cell.giftTitle.text = allGifts[indexPath.row].name
        cell.giftDetail.text = allGifts[indexPath.row].description
        cell.pointsLabel.text = String(allGifts[indexPath.row].remainingPoints)
        if let imgUrl = URL(string:allGifts[indexPath.row].image) {
            cell.giftImageView.kf.setImage(with: imgUrl)
        }
        return cell
    }
}
