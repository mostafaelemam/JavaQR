//
//  ItemViewController.swift
//  JavaQR
//
//  Created by Intrazero on 21/12/2023.
//

import UIKit
import Lottie

class ItemViewController: UIViewController {
    
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var scrollableView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemDetailLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var detailedMessageLabel: UILabel!
    @IBOutlet weak var getGiftBtn: UIButton!
    
    var rewardAnimation : LottieAnimationView = .init()
    override func viewDidLoad() {
        super.viewDidLoad()
        itemImageView.image = UIImage(named: "iced-coffee")
        itemLabel.text = "Iced Coffee Latte"
        itemDetailLabel.text = "Lorem ipsum dolor sit amet, cons adipiscing elit."
        pointsLabel.text = "400/431 Points"
        giftBtnAppeared()
        setupAnimation()
    }
// to show the gift button and change the labels (For checking)
    func giftBtnAppeared() {
        myScrollView.isScrollEnabled = true
        getGiftBtn.isHidden = false
        messageLabel.text  = "You Can Redeem This Gift"
        detailedMessageLabel.text = "Exchange 431 points to get this gift from Starbucks"
    }
    func setupAnimation() {
        rewardAnimation.animation = .named("gift box")
        rewardAnimation.frame = CGRect(x: 0, y: 330, width: view.bounds.width, height: 110)
        rewardAnimation.contentMode = .scaleAspectFit
        rewardAnimation.loopMode = .loop
        rewardAnimation.animationSpeed = 0.7
        scrollableView.addSubview(rewardAnimation)
        rewardAnimation.play()
    }
    @IBAction func getBtnPressed() {
        print("Pressed")
    }
}
