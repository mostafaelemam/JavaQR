//
//  FAQViewController.swift
//  JavaQR
//
//  Created by Intrazero on 19/12/2023.
//

import UIKit

class FAQViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var faqViewModel : FAQViewModel?
    private var faqs: FAQData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.string(forKey: K.Key.token))
        loadData()
        
        tableView.register(FAQTableViewCell.nib, forCellReuseIdentifier: FAQTableViewCell.identifier)
    }
    private func loadData() {
        faqViewModel = FAQViewModel(faqServiceProtocol: FAQService())
        if let faqVM = faqViewModel {
            faqVM.getFaqs()
            faqVM.responseSuccessCallback = { [weak self] in
                self?.faqs = faqVM.faqData
                self?.tableView.reloadData()
            }
        }
    }

}
// MARK: - TableView Methods
extension FAQViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.faqs?.data.data.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FAQTableViewCell.identifier, for: indexPath) as! FAQTableViewCell
        guard let allFaqs = self.faqs?.data.data else {fatalError("No Questions to display")}
        cell.titleLabel.text = allFaqs[indexPath.row].question
        
        if allFaqs[indexPath.row].isExpanded  {
            cell.rightImageView.image = UIImage(named: "arrow-up")
            cell.detailsLabel.text = allFaqs[indexPath.row].answer
        } else {
            cell.rightImageView.image = UIImage(named: "arrow-down")
            cell.detailsLabel.text = ""
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.faqs?.data.data[indexPath.row].isExpanded.toggle()
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
