//
//  AccountViewController.swift
//  JavaQR
//
//  Created by Intrazero on 18/12/2023.
//

import UIKit
import FirebaseAuth

class AccountViewController: UIViewController {
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet var superView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func logoutPressed(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.7) {
            self.blurView.alpha = 0.7
            self.popupView.alpha = 1.0
        }
        tabBarController?.tabBar.isUserInteractionEnabled = false
    }
    @IBAction func confirmLogout(_ sender: UIButton){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            UserDefaults.standard.removeObject(forKey: K.Key.token)
            presentWelcomeScreen()
            
        } catch let signOutError as NSError {
          print("Error signing out: %@", signOutError)
            MyController.showBanner(title: Localized.error, message: signOutError.localizedDescription)
        }
    }
    @IBAction func cancelLogout(_ sender: UIButton){
        self.blurView.alpha = 0
        self.popupView.alpha = 0
        tabBarController?.tabBar.isUserInteractionEnabled = true
    }
//    Present Welcome Screen again when user logged out.
    private func presentWelcomeScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: K.Key.welcomeVC)
        let navigationController = UINavigationController(rootViewController: welcomeVC)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.tintColor = .black
        present(navigationController, animated: true)
    }
}
