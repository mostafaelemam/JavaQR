//
//  PointsHistoryViewController.swift
//  JavaQR
//
//  Created by Intrazero on 18/12/2023.
//

import UIKit

class PointsHistoryViewController: UIViewController {

    @IBOutlet weak var segmentedCtrl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(PointTableViewCell.nib, forCellReuseIdentifier: PointTableViewCell.identifier)
        setupSegmentedCtrl()
        
    }
    private func setupSegmentedCtrl() {
        segmentedCtrl.setTitleTextAttributes([.foregroundColor : UIColor.white], for: .selected)
        segmentedCtrl.setTitleTextAttributes([ .font : UIFont(name: "Inter-Regular_Bold", size: 13)!], for: .normal)
    }
    
    
    @IBAction func segmentedCtrlValueChanged(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
}
// MARK: - TableView DataSource
extension PointsHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PointTableViewCell.identifier, for: indexPath) as! PointTableViewCell
        switch segmentedCtrl.selectedSegmentIndex {
        case 0:
            cell.pointsLabel.text = "+431"
            cell.pointsLabel.textColor = UIColor(named: "Success-300")
        case 1:
            cell.pointsLabel.text = "-431"
            cell.pointsLabel.textColor = UIColor(named: "Warning-300")
        default:
            return cell
        }
        return cell
    }
}
