//
//  WelcomeViewController.swift
//  JavaQR
//
//  Created by Intrazero on 13/12/2023.
//

import UIKit

class ChooseLanguageViewController: UIViewController {
    @IBOutlet weak var arabicView: UIView!
    @IBOutlet weak var englishView: UIView!
    
    @IBOutlet weak var arabicLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    
    @IBOutlet weak var arabicImageView: UIImageView!
    @IBOutlet weak var englishImageView: UIImageView!
    
    private var currentLang = "en"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func chooseBtnPressed(_ sender: UIButton) {
        UserDefaults.standard.setValue([currentLang], forKey: "AppleLanguages")
        showRestartAlert()
    }
    
    @IBAction func LanguageChoosen(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            currentLang = "ar"

            changeSelectedView(isSelected: true, view: arabicView, label: arabicLabel, imgView: arabicImageView)
            changeSelectedView(isSelected: false, view: englishView, label: englishLabel, imgView: englishImageView)
        case 1:
            currentLang = "en"

            changeSelectedView(isSelected: false, view: arabicView, label: arabicLabel, imgView: arabicImageView)
            changeSelectedView(isSelected: true, view: englishView, label: englishLabel, imgView: englishImageView)
        default:
            return
        }
    }
//    Change Selected View Background
    private func changeSelectedView(isSelected: Bool,view: UIView, label: UILabel, imgView : UIImageView) {
        
        if isSelected {
            view.backgroundColor = UIColor(named: "Blue")
            label.textColor = .white
            label.font = UIFont(name: "Inter-Regular_SemiBold", size: 16)
            imgView.image = UIImage(systemName: "checkmark.square")
            imgView.tintColor = .white
        } else {
            view.backgroundColor = UIColor(named: "Gray 5")
            label.textColor =  UIColor(named: "Gray 3")
            label.font = UIFont(name: "Inter-Regular", size: 16)
            imgView.image = UIImage(systemName: "square")
            imgView.tintColor = UIColor(named: "Gray 3")
        }
    }
    private func showRestartAlert() {
        let alert = UIAlertController(title: "App Restart Required", message: "Please restart the app for the language change to take effect.", preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Restart", style: .default) { (_) in
            // Exit the app
            exit(0)
        }
        alert.addAction(restartAction)
        self.present(alert, animated: true, completion: nil)
    }
}
