//
//  WelcomeViewController.swift
//  JavaQR
//
//  Created by Intrazero on 14/12/2023.
//

import UIKit
import Lottie

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    var handAnimation : LottieAnimationView = .init()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimation()
    }
    func setupAnimation() {
        handAnimation.animation = .named("hand")
//        for small mob H = 667 , for large mob H = 852
        if view.bounds.height > 667 {
            handAnimation.frame = CGRect(x: 0, y: 53, width: view.bounds.width, height: 0.78 * view.bounds.height)
        } else {
            handAnimation.frame = CGRect(x: 0, y: 50, width: view.bounds.width, height: 0.75 * view.bounds.height)
        }
        handAnimation.contentMode = .scaleAspectFill
        handAnimation.animationSpeed = 0.7
        handAnimation.loopMode = .playOnce
        headerView.addSubview(handAnimation)
        handAnimation.play()
    }
}
