//
//  LoginViewController.swift
//  JavaQR
//
//  Created by Intrazero on 14/12/2023.
//

import UIKit
import CountryPicker
import FirebaseAuth

class LoginViewController: UIViewController {
    @IBOutlet weak var countryFlagImgView: UIImageView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryPickerView: CountryPicker!
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var sendOTP: UIButton!
    
    private let authViewModel = AuthViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    private func setupUI() {
        phoneTextField.becomeFirstResponder()
        phoneTextField.padding(left: 15, right: 0)
        phoneTextField.delegate = self
        countryPickerView.countryPickerDelegate = self
        countryPickerView.showPhoneNumbers = true
        countryPickerView.setCountry("EG")
    }
    @IBAction func codeBtnPressed(_ sender: UIButton) {
        countryPickerView.isHidden = !countryPickerView.isHidden
        phoneTextField.resignFirstResponder()
    }
    @IBAction func sendOTPBtnPressed(_ sender: UIButton) {

        guard let phoneNumber = phoneTextField.text,
              let code  = countryPickerView.currentCountry?.phoneCode
        else { return }
        if !phoneNumber.isEmpty {
            MyController.showLoading(parentView: sender, color: .black, tag: K.ViewTag.loading)
            authViewModel.sendOTP(countryCode: code, phone: phoneNumber) { [weak self] error in
                MyController.hideLoading(viewTag: K.ViewTag.loading, parentView: sender)
                if error == nil {
                    self?.performSegue(withIdentifier: K.Segue.otp, sender: self)
                }
            }
        }
        else {
            MyController.showBanner(title: Localized.error, message: Localized.phoneIsRequired)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? OTPViewController {
            destinationVC.phoneNumber = phoneTextField.text
            destinationVC.countryCode = countryPickerView.currentCountry?.phoneCode
        }
    }
}
// MARK: - TextField Delegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        countryPickerView.isHidden = true
    }
}
// MARK: - CountryPickerDelegate
extension LoginViewController: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.codeButton.setTitle(phoneCode, for: .normal)
        self.countryFlagImgView.image = flag
    }
}

