//
//  ViewController.swift
//  JavaQR
//
//  Created by Intrazero on 13/12/2023.
//

import UIKit
import Lottie

class SplashViewController: UIViewController {
    var javaLogoAnimation : LottieAnimationView = .init()
    var backgroundAnimation : LottieAnimationView = .init()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            IF it's first time to open the app => Language Selection
            if AppDelegate().shouldShowLanguageSelection() {
                self.presentLanguageSelectionScreen()
            }
            if let token = UserDefaults.standard.string(forKey: K.Key.token) , !token.isEmpty {
                self.performSegue(withIdentifier: K.Segue.home, sender: self)
            } else {
                self.performSegue(withIdentifier: K.Segue.welcome, sender: self)
            }
            }
        }
//    Presenting the Choose Language Screen for First time ONLY for User
    private func presentLanguageSelectionScreen() {
        AppDelegate().setLanguageSelectionShown()
        self.performSegue(withIdentifier: K.Segue.changeLanguage, sender: self)
    }
    private func setupAnimation() {
//        Background Animation
        
        backgroundAnimation.animation = .named("shapes")
        backgroundAnimation.frame = view.bounds
        backgroundAnimation.contentMode = .center
        backgroundAnimation.loopMode = .playOnce
        backgroundAnimation.animationSpeed = 0.8
        view.addSubview(backgroundAnimation)
        backgroundAnimation.play()
//        JavaLogo Animation
        
        javaLogoAnimation.animation = .named("javalogo")
        javaLogoAnimation.frame = view.bounds
        javaLogoAnimation.contentMode = .scaleAspectFit
        javaLogoAnimation.loopMode = .playOnce
        javaLogoAnimation.animationSpeed = 0.8
        view.addSubview(javaLogoAnimation)
        javaLogoAnimation.play()
    }
}

