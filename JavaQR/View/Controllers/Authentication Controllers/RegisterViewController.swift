//
//  RegisterViewController.swift
//  JavaQR
//
//  Created by Intrazero on 16/12/2023.
//

import UIKit
import CountryPicker
import Alamofire
import SwiftMessages


class RegisterViewController: UIViewController {
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var countryFlagImgView: UIImageView!
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var countryPickerView: CountryPicker!
    
    @IBOutlet weak var registerButton: UIButton!
    
    private let authViewModel = AuthViewModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    func updateUI() {
        firstNameField.padding(left: 15, right: 0)
        lastNameField.padding(left: 15,  right: 0)
        phoneTextField.padding(left: 15, right: 0)
        
        firstNameField.delegate = self
        lastNameField.delegate = self
        phoneTextField.delegate = self
        countryPickerView.countryPickerDelegate = self
//        Setting CountryPickerView
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)
        countryPickerView.theme = theme
        countryPickerView.showPhoneNumbers = true
        countryPickerView.setCountry("EG")

    }
    @IBAction func codeBtnPressed(_ sender: UIButton) {
        countryPickerView.isHidden = !countryPickerView.isHidden
        phoneTextField.resignFirstResponder()
    }
    @IBAction func registerBtnPressed(_ sender: UIButton) {
        guard let fname = firstNameField.text,
              let lname = lastNameField.text,
              let phone = phoneTextField.text,
              let code  = countryPickerView.currentCountry?.phoneCode
        else { return }
        
        if !fname.isEmpty,!lname.isEmpty,!phone.isEmpty {
            MyController.showLoading(parentView: sender, color: .black, tag: K.ViewTag.loading)
            
            let user = User(firstName: fname, lastName: lname, countryCode: code, mobile: phone, fcm: "dfsdff")
            authViewModel.register(user: user) { [weak self] success, authModel, error in
                MyController.hideLoading(viewTag: K.ViewTag.loading, parentView: sender)
                if success {
                    self?.performSegue(withIdentifier: K.Segue.home, sender: self)
                }
            }
        } else {
            MyController.showBanner(title: Localized.error, message: Localized.emptyFields)
        }
    }
}
// MARK: - TextField Delegate
extension RegisterViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        countryPickerView.isHidden = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.returnKeyType {
        case .next:
            lastNameField.becomeFirstResponder()
        case .done:
            textField.resignFirstResponder()
        default:
            return true
        }
        return true
    }
}
// MARK: - CountryPickerDelegate
extension RegisterViewController: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.codeButton.setTitle(phoneCode, for: .normal)
        self.countryFlagImgView.image = flag
    }
}
