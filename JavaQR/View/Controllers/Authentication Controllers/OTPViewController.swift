//
//  OTPViewController.swift
//  JavaQR
//
//  Created by Intrazero on 17/12/2023.
//

import UIKit
import FirebaseAuth

class OTPViewController: UIViewController {
    @IBOutlet var countDownLabel: UILabel!
    @IBOutlet var resendOTPBtn: UIButton!
    @IBOutlet var verifyBtn: UIButton!
    
    @IBOutlet weak var otp1: UITextField!
    @IBOutlet weak var otp2: UITextField!
    @IBOutlet weak var otp3: UITextField!
    @IBOutlet weak var otp4: UITextField!
    @IBOutlet weak var otp5: UITextField!
    @IBOutlet weak var otp6: UITextField!
    
    private let authViewModel = AuthViewModel()

    var phoneNumber : String?
    var countryCode : String?
    
    var secondsRemaining = 59
    var timer : Timer?
    
    var otps = [UITextField]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countDownLabel.text = Localized.resendLabel(secondsRemaining)

        otps = [otp1,otp2,otp3,otp4,otp5,otp6]
        otps.forEach { $0.delegate = self }
        otp1.becomeFirstResponder()
        startTimer()
    }
    func startTimer() {
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    @objc func updateTimer() {
        if secondsRemaining > 0 {
            secondsRemaining -= 1
            countDownLabel.text = Localized.resendLabel(secondsRemaining)
        } else {
            timer?.invalidate()
            countDownLabel.isHidden = true
            resendOTPBtn.isHidden = false
        }
    }
    deinit {
        timer?.invalidate()
    }
    @IBAction func verifyBtnPressed(_ sender: UIButton!) {
        guard let phoneNumber = phoneNumber,
              let countryCode = countryCode
        else { return }
        MyController.showLoading(parentView: sender, color: .black, tag: K.ViewTag.loading)
        let verificationCode = otps.map({$0.text ?? ""}).reduce("",+)
        let user = User(firstName: nil, lastName: nil, countryCode: countryCode, mobile: phoneNumber, fcm: "dsdas")
        authViewModel.verifyOTP(with: verificationCode) { error in
            MyController.hideLoading(viewTag: K.ViewTag.loading, parentView: sender)
            if let error = error as? AuthErrorCode {
                switch error.code {
                case .invalidVerificationCode:
                    MyController.showBanner(title: Localized.error, message: Localized.invalidOTP)
                default:
                    MyController.showBanner(title: Localized.error, message: Localized.somethingWrong)
                }
            } else {
                self.authViewModel.login(user: user) { [weak self] success, authModel, error in
                    if success {
                        self?.performSegue(withIdentifier: K.Segue.home, sender: self)
                    }
                }
            }
        }
    }
    @IBAction func resendOTPBtn(_ sender: UIButton!) {
        countDownLabel.isHidden = false
        resendOTPBtn.isHidden = true
        secondsRemaining = 59
        countDownLabel.text = "Resend OTP in 00:\(secondsRemaining)s"
        startTimer()
//        Resending OTP
        if let phoneNumber = phoneNumber, let code = countryCode {
            authViewModel.sendOTP(countryCode: code, phone: phoneNumber) { _ in }
        }
    }
}
// MARK: -  UITextField Delegate
extension OTPViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.isEmpty {
            switch textField {
            case otp1:
                otp1.text = string
                otp2.becomeFirstResponder()
            case otp2:
                otp2.text = string
                otp3.becomeFirstResponder()
            case otp3:
                otp3.text = string
                otp4.becomeFirstResponder()
            case otp4:
                otp4.text = string
                otp5.becomeFirstResponder()
            case otp5:
                otp5.text = string
                otp6.becomeFirstResponder()
            case otp6:
                otp6.text = string
                textField.endEditing(true)
            default:
                return false
            }
            let verifyBtnShouldAppear = otps.allSatisfy {$0.text?.isEmpty == false}
            if verifyBtnShouldAppear {
                self.verifyBtn.alpha = 1
                self.verifyBtn.isEnabled = true
            }
        } else {
            self.verifyBtn.alpha = 0.3
            self.verifyBtn.isEnabled = false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = .blue
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.borderColor = .clear
    }
}

