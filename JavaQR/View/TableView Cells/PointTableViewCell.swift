//
//  PointTableViewCell.swift
//  JavaQR
//
//  Created by Intrazero on 17/12/2023.
//

import UIKit

class PointTableViewCell: UITableViewCell {
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib : UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
