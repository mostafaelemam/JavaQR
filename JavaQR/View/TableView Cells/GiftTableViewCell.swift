//
//  GiftTableViewCell.swift
//  JavaQR
//
//  Created by Intrazero on 21/12/2023.
//

import UIKit

class GiftTableViewCell: UITableViewCell {
    
    @IBOutlet weak var giftTitle: UILabel!
    
    @IBOutlet weak var giftDetail: UILabel!
    
    @IBOutlet weak var giftImageView: UIImageView!
    
    @IBOutlet weak var pointsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    //    Registeration
    static var identifier : String {
        return String(describing: self)
    }
    static var nib : UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

}

