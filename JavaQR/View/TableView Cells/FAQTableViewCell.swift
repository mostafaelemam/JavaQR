//
//  FAQTableViewCell.swift
//  JavaQR
//
//  Created by Intrazero on 19/12/2023.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var detailsLabel: UILabel!
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib : UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
