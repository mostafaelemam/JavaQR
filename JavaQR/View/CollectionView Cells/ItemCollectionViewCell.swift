//
//  ItemCollectionViewCell.swift
//  JavaQR
//
//  Created by Intrazero on 17/12/2023.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var shopLabel: UILabel!
    @IBOutlet weak var getBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib : UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
