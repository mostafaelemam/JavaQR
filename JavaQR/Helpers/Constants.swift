//
//  Constants.swift
//  JavaQR
//
//  Created by Intrazero on 14/12/2023.
//

import Foundation
import Alamofire

struct K {
    // MARK: -  Segue Names
    
    struct Segue {
        static let home           = "goToHome"
        static let changeLanguage = "goToChangeLanguage"
        static let shop           = "goToShop"
        static let item           = "goToItem"
        static let otp            = "goToOTP"
        static let login          = "goToLogin"
        static let welcome        = "goToWelcome"
    }
    
    // MARK: - Key&Identifiers Names
    
    struct Key {
        static let firstName    = "firstName"
        static let lastName     = "lastName"
        static let mobile       = "mobile"
        static let countryCode  = "countryCode"
        static let token        = "token"
        static let authID       = "authVerificationID"
        static let welcomeVC    = "WelcomeViewController"
    }
    
    // MARK: - View Tags
    
    struct ViewTag {
        static let loading      = 999
    }
    
    // MARK: - Apis
    struct Api {
        private static let contentType  = "application/x-www-form-urlencoded"
        private static let acceptType   = "application/json"
        private static let lang         = Locale.current.languageCode ?? "en"
        private static let token        = UserDefaults.standard.string(forKey: K.Key.token) ?? ""
        static var headers: HTTPHeaders = ["Content-Type": contentType,
                                           "Accept": acceptType,
                                           "Authorization": "Bearer \(token)",
                                           "Accept-Language": lang]
        static let url      = "https://javaqr.com/api/"
        static let register = "register"
        static let login    = "login/token"
        static let faqs     = "client/faqs"
        static let gifts    = "client/allGifts"
    }
}
