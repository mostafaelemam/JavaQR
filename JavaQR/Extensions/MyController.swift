import UIKit
import SwiftMessages

class MyController {
//    Showing alert
    static func showAlert(title: String = "Error", alertMessage: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            return
        }))
        return alert
    }
//    Rounded View Corners for top or bottom only
    static func roundedOnlyTop(for view: UIView)  {
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    static func roundedOnlyBottom(for view: UIView)  {
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
//    getting Font Names
    static func getFontNames() {
        for family in UIFont.familyNames {
            let name = UIFont.fontNames(forFamilyName: family)
            print("family= \(family) - name= \(name)")
        }
    }
//    Padding for UItextFields
    static func padding(_ textfields: [UITextField],left: CGFloat, right: CGFloat) {
        for textfield in textfields {
            let leftPaddingView = UIView(frame: CGRectMake(0, 0, left, textfield.frame.height))
            let rightPaddingView = UIView(frame: CGRectMake(0, 0, right, textfield.frame.height))
            
            textfield.leftView = leftPaddingView
            textfield.leftViewMode = .always
            textfield.rightView = rightPaddingView
            textfield.rightViewMode = .always
        }
    }
//    Eye Symbol in Password UITextFields
    static func changePasswordVisiblity(eyeButton: UIButton , passwordField: UITextField) {
        passwordField.isSecureTextEntry = !passwordField.isSecureTextEntry
       
        if passwordField.isSecureTextEntry {
            eyeButton.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        } else {
            eyeButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        }
    }
//    Banner Message
    static func showBanner(title: String, message: String , duration: Double = 1, theme: Theme = .error) {
        let view = MessageView.viewFromNib(layout: .cardView)
        var config = SwiftMessages.Config()
        
        config.duration = .seconds(seconds: duration)
        view.button?.isHidden = true
        view.configureTheme(theme)
        view.configureDropShadow()
        view.configureContent(title: title, body: message)
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 0)
        view.cornerRadius = 10
        SwiftMessages.show(config: config, view: view)
    }
//    Showing Activity Indicator
    static func showLoading(parentView: UIView , color: UIColor , tag : Int) {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = color
        activityIndicator.tag = tag
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: parentView.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: parentView.centerYAnchor),
            activityIndicator.heightAnchor.constraint(equalTo: parentView.heightAnchor, multiplier: 0.4),
            activityIndicator.widthAnchor.constraint(equalTo: parentView.widthAnchor, multiplier: 0.2)
        ])
        activityIndicator.startAnimating()
        parentView.isUserInteractionEnabled = false
        parentView.alpha = 0.5
    }
//    Hiding Activity Indicator
    static func hideLoading(viewTag tag : Int, parentView: UIView) {
        if let activityIndicator = parentView.viewWithTag(tag) as? UIActivityIndicatorView {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            parentView.isUserInteractionEnabled = true
            parentView.alpha = 1
        }
    }


}
