import UIKit

extension UIView {
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            if let colorCG = layer.borderColor {
                return UIColor(cgColor: colorCG)
            }
            return nil
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
extension UITextField {
    func padding(left: CGFloat, right: CGFloat) {
        let leftPaddingView = UIView(frame: CGRectMake(0, 0, left, frame.height))
        let rightPaddingView = UIView(frame: CGRectMake(0, 0, right, frame.height))
        
        leftView = leftPaddingView
        leftViewMode = .always
        rightView = rightPaddingView
        rightViewMode = .always
    }
    @IBInspectable var placeholderColor: UIColor? {
        get {
            return self.textColor
        }
        set {
            attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : newValue ?? .gray])
        }
    }
}
