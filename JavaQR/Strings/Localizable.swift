import Foundation


// MARK: - Strings

internal enum Localized {
  /// Some Fields May be Empty!
  internal static let emptyFields = Localized.tr("Localizable", "emptyFields", fallback: "Some Fields May be Empty!")
  /// Error
  internal static let error = Localized.tr("Localizable", "error", fallback: "Error")
  /// The interaction is cancelled by the user.
  internal static let interactionCancelled = Localized.tr("Localizable", "interactionCancelled", fallback: "The interaction is cancelled by the user.")
  /// Invalid Verification Code , Please try again or press Resend Verification Code
  internal static let invalidOTP = Localized.tr("Localizable", "invalidOTP", fallback: "Invalid Verification Code , Please try again or press Resend Verification Code")
  /// Invalid Phone Number!
  internal static let invalidPhone = Localized.tr("Localizable", "invalidPhone", fallback: "Invalid Phone Number!")
  /// Please Enter Phone Number!
  internal static let phoneIsRequired = Localized.tr("Localizable", "phoneIsRequired", fallback: "Please Enter Phone Number!")
  /// Something Went Wrong!
  internal static let somethingWrong = Localized.tr("Localizable", "somethingWrong", fallback: "Something Went Wrong")
  internal static let faqsNotFound = Localized.tr("Localizable", "faqsNotFound", fallback: "Could not Fetch the FAQs")
  /// Resend OTP Code in %d Seconds.
  internal static func resendLabel(_ p1: Int) -> String {
    return Localized.tr("Localizable", "resendLabel", p1, fallback: "Resend OTP Code in %d Seconds.")
  }
  /// UnAuthorized
  internal static let unauthorized = Localized.tr("Localizable", "unauthorized", fallback: "Unauthorized, Please Login")
}

// MARK: - Implementation Details

extension Localized {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
